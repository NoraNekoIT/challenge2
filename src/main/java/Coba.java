import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Coba {
    static String delimeter = ";";
    static List<Integer> list = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static Boolean state = true;
    static String lokasiDataCsv = "C:\\Users\\bunsm\\Desktop\\Semester6\\Backend Java x Binar Academy\\Challenge2\\data_sekolah.csv";

    public static void main(String[] args) {
        read(lokasiDataCsv);
        while (state) {
            menuUtama();
        }
        input.close();
    }

    public static void menuUtama() {
        System.out.println("\n\nAplikasi Pengolah Nilai Siswa");
        System.out.println("Letakkan file csv di :\n" + lokasiDataCsv);
        System.out.println("Pilih menu: ");
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata , median");
        System.out.println("3. Generate kedua file");
        System.out.println("4. Keluar ");
        int number = input.nextInt();
        PilihanMenu(number);
//        input.close();
    }

    static void PilihanMenu(int n) {
        if (n == 1) {
            write("data_sekolah_modus.txt", 1);
        } else if (n == 2) {
            write("data_sekolah_mean_median.txt", 2);
        } else if (n == 3) {
            write("data_sekolah_modus.txt", 1);
            write("data_sekolah_mean_median.txt", 2);
        } else if (n == 4) {
            state = false;
            System.exit(0);
        } else {
            System.out.println("input tidak diketahui");
            menuUtama();
        }
    }

    public static Map<Integer, Integer> getFrequency(List<Integer> listTemp) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        listTemp.stream()
                .distinct()
                .forEach(
                        element -> frequencyMap.put(
                                element,
                                Collections.frequency(list, element)
                        )
                );
        return frequencyMap;
    }

    public static int getModus(List<Integer> listTemp){
        int maxValue = 0, maxCount = 0;

        for (Integer integer : listTemp) {
            int count = 0;
            for (int j = 0; j < list.size(); ++j) {
                if (Objects.equals(listTemp.get(j), integer)) ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = integer;
            }
        }

        return maxValue;
    }

    public static double getMean(List<Double> listTempMean) {
        double temp =0;
        for (Double doubleTemp : listTempMean) {
            temp +=doubleTemp;
//            System.out.println(temp);
        }
        return temp/listTempMean.size() ;
    }

    public static double getMedian(List<Double> listTempMedian) {
        Collections.sort(listTempMedian);
        double median;
        if (listTempMedian.size() % 2 == 1) {
            median = listTempMedian.get(list.size() / 2);
        } else {
            median = (listTempMedian.get(list.size() / 2) + listTempMedian.get((list.size() / 2) - 1)) / 2;
        }
        return median;
    }

    public static void read(String csvFile) {
        try {
            File file = new File(csvFile);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;

            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimeter);
                for (String tempStr : tempArr) {
                    if (Pattern.matches("\\d+", tempStr)) {
                        list.add(Integer.valueOf(tempStr + ""));
                    }
                }
            }
//            System.out.println(list);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void write(String txtFile, int noDipilih) {
        try {
            File file = new File(txtFile);
            if (noDipilih == 1) {
                if (file.createNewFile()) {
                    System.out.println("file baru berhasil dibuat di\n"+
                            file.getAbsolutePath()
                            );
                }
                FileWriter writer = new FileWriter(file);
                BufferedWriter bwr = new BufferedWriter(writer);
                bwr.write("Berikut hasil Pengolahan Nilai:\n");
                bwr.write("Nilai " + "|" + "Frekuensi \n");

                for (Integer i : getFrequency(list).keySet()) {
                    bwr.write(" " + i + "   |  " + getFrequency(list).get(i) + "\n");
                }
                bwr.newLine();
                bwr.flush();
                bwr.close();
                System.out.println("Berhasil menulis file di path \n" +
                                file.getAbsolutePath()
                        );
            } else if (noDipilih == 2) {
                if (file.createNewFile()) {
                    System.out.println("file baru berhasil dibuat di\n"+
                            file.getAbsolutePath()
                            );
                }
                FileWriter writer = new FileWriter(file);
                BufferedWriter bwr = new BufferedWriter(writer);
                bwr.write("Berikut hasil Pengolahan Nilai:\n");
                bwr.write("Berikut hasil sebaran data nilai \n");
                bwr.write("Mean : " + getMean(list.parallelStream().mapToDouble(i -> i).boxed().collect(Collectors.toList())) + "\n");
                bwr.write("Median : " + getMedian(list.parallelStream().mapToDouble(i -> i).boxed().collect(Collectors.toList())) + "\n");
                bwr.write("Modus : " + getModus(list) + "\n");
                bwr.newLine();
                bwr.flush();
                bwr.close();
                System.out.println("Berhasil menulis file di path \n"+
                                file.getAbsolutePath()
                        );
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
